import logging
import string
from typing import List

from tape.audio.api import SILENCE_LONG, AudioStreamInterruption
from tape.file.api import ByteListener, BlockListener, InvalidBlockType, InvalidCRCError, DataBlock
from tape.log.dump import dump

PRINTABLE = [c for c in string.printable if ord(c) > 0x20]


def dump_block(block: DataBlock, callback):
    callback('=================================================================================')
    callback('Block Type: %02Xh     Block Size: %02Xh' % (block.type, len(block.body)))
    callback('---------------------------------------------------------------------------------')

    for line in dump(block.body):
        callback(line)

    callback('=================================================================================')
    callback('')


class BlockPrinter(BlockListener):
    def process_block(self, block: DataBlock):
        dump_block(block, print)


class BlockParser(ByteListener):
    def __init__(self, listeners: List[BlockListener]):
        self._listeners = listeners
        self._buffer = None
        self._logger = logging.getLogger('file.block')

    def process_byte(self, value: int):
        # self._logger.debug('Current block bytes: %s  Additional byte: %02Xh'
        #                    % (str(['%02Xh' % v for v in self._buffer]) if self._buffer is not None else 'N/A', value))

        if self._buffer is None:
            if value == 0x3c:
                self._buffer = []

        else:
            self._buffer.append(value)

            if len(self._buffer) > 1 and len(self._buffer) == self._buffer[1] + 3:

                type = self._buffer[0]
                blen = self._buffer[1]
                bcrc = self._buffer[-1]
                body = self._buffer[2:-1]

                if type not in [0x00, 0x01, 0xFF]:
                    raise InvalidBlockType(type)

                if self._checksum(self._buffer[0:-1]) != bcrc:
                    raise InvalidCRCError()

                block = DataBlock(type, body)
                self.log_block(block)

                self._buffer = None

                for l in self._listeners:
                    l.process_block(block)

    def process_silence(self):
        if self._buffer is not None:
            self._buffer = None
            raise AudioStreamInterruption('Unexpected silence while reading block')

    def _checksum(self, bytes: List[int]):
        return sum(bytes) % 256

    def log_block(self, block: DataBlock):
        self._logger.debug('=================================================================================')
        self._logger.debug('Block Type: %02Xh     Block Size: %02Xh' % (block.type, len(block.body)))
        self._logger.debug('---------------------------------------------------------------------------------')

        for line in dump(block.body):
            self._logger.debug(line)

        self._logger.debug('=================================================================================')
        self._logger.debug('')


class BlockSerializer(BlockListener):
    def __init__(self, listeners: List[ByteListener]):
        self._listeners = listeners

    def process_block(self, block: DataBlock):
        if block.type == 0:
            self.notify(SILENCE_LONG)
            for i in range(128):
                self.notify(0x55)

        self.notify(0x3C)
        self.notify(block.type)
        self.notify(len(block.body))

        for b in block.body:
            self.notify(b)

        self.notify(block.checksum)

        if block.type == 0x00:
            self.notify(SILENCE_LONG)
            for i in range(128):
                self.notify(0x55)
        elif block.type == 0x01:
            self.notify(0x55)
            self.notify(0x55)
        elif block.type == 0xFF:
            self.notify(0x55)
            self.notify(SILENCE_LONG)

    def notify(self, b: int):
        for l in self._listeners:
            l.process_byte(b)
