import logging
from typing import List

from tape.audio.api import BitListener
from tape.file.api import ByteListener

SYNC_BYTE = 0x3c


class ByteDecoder(BitListener):
    def __init__(self, listeners: List[ByteListener]):
        self._buffer = []
        self._listeners = listeners
        self._synchronized = False
        self._logger = logging.getLogger('decoder.byte')

    def process_bit(self, value: int):
        # self._logger.debug('Processing bit %d with buffer %s' % (value, str(self._buffer)))
        self._buffer.append(value)

        if len(self._buffer) == 8:
            value = sum([self._buffer[i] * pow(2, i) for i in range(0, 8)])

            if not self._synchronized:
                if value == SYNC_BYTE:
                    self._synchronized = True
                else:
                    self._buffer = self._buffer[1:]

            if self._synchronized:

                for l in self._listeners:
                    l.process_byte(value)

                self._buffer = []

    def process_silence(self):
        self._buffer = []
        self._synchronized = False

        for l in self._listeners:
            l.process_silence()


class ByteSerializer(ByteListener):

    def __init__(self, listeners: List[BitListener]):
        self._listeners = listeners

    def process_byte(self, byte: int):

        bits = []

        if byte < 0:
            bits.append(byte)
        else:
            for b in range(8):
                bit = (byte & (1 << b)) >> b
                bits.append(bit)

        for l in self._listeners:
            for b in bits:
                l.process_bit(b)
