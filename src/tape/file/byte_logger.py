import logging

from tape.file.api import ByteListener


class ByteLogger(ByteListener):
    def __init__(self):
        self._logger = logging.getLogger('file.bytes')

    def process_byte(self, value: int):
        self._logger.debug('%02x' % (value))
