import logging
from typing import List

from tape.file.api import BlockListener, TapeFileListener, DataBlock, TapeFile, UnexpectedBlockType


class TapeFilePrinter(TapeFileListener):
    def process_file(self, file: TapeFile):
        print('  +---------------------\\')
        print('  |                     |\\')
        print('  | File: %8s      |_\\' % (file.fname))
        print('  | Size: %5d bytes     |' % (len(file.fbody)))
        print('  | Type: %02Xh             |' % (file.ftype))
        print('  | Data: %02Xh             |' % (file.fdatatype))
        print('  |  Gap: %02Xh             |' % (file.fgap))
        print('  |                       |')
        print('  +-----------------------+')
        print()

class TapeFileLoader(BlockListener):
    def __init__(self, listeners: List[TapeFileListener]):
        self._listeners = listeners
        self._blocks = []
        self._logger = logging.getLogger('file.tapefile')

    def process_block(self, block: DataBlock):

        if block.type == 0x00 and len(self._blocks) > 0:
            raise UnexpectedBlockType(block.type, 0)

        if block.type in [0x01, 0xFF] and len(self._blocks) == 0:
            raise UnexpectedBlockType(block.type, 0)

        self._blocks.append(block)

        if block.type == 0xFF:

            file = TapeFile(self._blocks)

            self._logger.debug('  +---------------------\\')
            self._logger.debug('  |                     |\\')
            self._logger.debug('  | File: %8s      |_\\' % (file.fname))
            self._logger.debug('  | Size: %5d bytes     |' % (len(file.fbody)))
            self._logger.debug('  | Type: %02Xh             |' % (file.ftype))
            self._logger.debug('  | Data: %02Xh             |' % (file.fdatatype))
            self._logger.debug('  |  Gap: %02Xh             |' % (file.fgap))
            self._logger.debug('  |                       |')
            self._logger.debug('  +-----------------------+')

            for l in self._listeners:
                l.process_file(file)

            self._blocks = []


class TapeFileSerializer(TapeFileListener):
    def __init__(self, listeners: List[BlockListener]):
        self._listeners = listeners

    def process_file(self, file: TapeFile):
        for block in file.blocks:
            for l in self._listeners:
                l.process_block(block)
