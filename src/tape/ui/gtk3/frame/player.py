from gi.repository import Gtk, GLib

from tape.audio.signal_out import AudioPlayerProgress
from tape.file.api import TapeFile


class ModalPlayer(Gtk.Dialog):
    def __init__(self, window: Gtk.Window, tape: TapeFile):
        Gtk.Dialog.__init__(self)

        # Avoid duplicate destruction if progress target is reported several times
        self.destroyed = False

        self.set_modal(True)
        self.set_transient_for(window)
        self.set_decorated(False)

        self.set_default_size(250, 100)

        self.progressbar = Gtk.ProgressBar()

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=12)
        box.add(Gtk.Label('Playing file %s' % tape.fname))
        box.add(self.progressbar)
        self.get_content_area().add(box)

        self.show_all()

    def on_progress(self, progress: AudioPlayerProgress):
        GLib.idle_add(self.progressbar.set_fraction, float(progress.progress) / float(progress.target))

        if not self.destroyed and progress.progress == progress.target:
            GLib.idle_add(self.destroy)
            self.destroyed = True
