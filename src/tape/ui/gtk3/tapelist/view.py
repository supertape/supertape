from gi.overrides.Gtk import TreeView

from tape.ui.gtk3.tapelist.repository import TapeRepositoryWrapperStore


class TapeView(TreeView):
    def __init__(self, store: TapeRepositoryWrapperStore):
        super().__init__(store)
