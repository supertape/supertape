import logging
from typing import Optional, List

from tape.audio.api import AudioLevelListener
from tape.audio.device import get_device
from tape.audio.level import AudioLevelCalculator
from tape.audio.modulation import AudioDemodulator
from tape.audio.signal_in import AudioInput
from tape.audio.signal_out import AudioPlayerObserver
from tape.file.api import TapeFileListener, TapeFile
from tape.file.block import BlockParser
from tape.file.bytes import ByteDecoder
from tape.file.play import play_file
from tape.file.tapefile import TapeFileLoader
from tape.repository.api import TapeFileRepository
from tape.repository.repo_db import ZodbRepository


class ApplicationFileListener(TapeFileListener):

    def __init__(self, repository: TapeFileRepository):
        self.repository = repository
        self._logger = logging.getLogger('control.main')

    def process_file(self, file: TapeFile):
        self._logger.info('Added file %s to repository %s' % (file.fname, self.repository))
        self.repository.add_tape_file(file)


class UserInterfaceController:
    def __init__(self, device: Optional[int], storage: str):
        self._logger = logging.getLogger('control.main')
        self._audio_in = None
        self._device = device if device is not None else get_device().get_default_device()
        self._levelmeter = AudioLevelCalculator([])
        self._repository = ZodbRepository(repository_file=storage, observers=[])
        self._filelistener = ApplicationFileListener(repository=self._repository)
        self.reset()

    def get_repository(self):
        return self._repository

    def register_level_listener(self, listener: AudioLevelListener):
        self._levelmeter.set_listeners([listener])

    def get_audio_devices(self) -> List:
        return get_device().get_audio_devices()

    def get_selected_device(self) -> int:
        return self._device

    def set_selected_device(self, selection: int):
        self._device = selection
        self.reset()

    def reset(self):
        filereader = TapeFileLoader([self._filelistener])
        blockreader = BlockParser([filereader])
        decoder = ByteDecoder([blockreader])

        demodulation = AudioDemodulator([decoder], get_device().get_sample_rate(self._device))

        if self._audio_in is not None:
            self._audio_in.stop()

        self._audio_in = AudioInput(listeners=[self._levelmeter, demodulation], device=self._device)
        self._audio_in.start()

    def play_tape(self, tape: TapeFile, observer: AudioPlayerObserver):
        self._logger.debug('Playing %s' % tape)
        play_file(tape, observer=observer, device=self.get_selected_device())

    def remove_tape(self, tape: TapeFile):
        self._logger.debug('Removing %s' % tape)
        self._repository.remove_tape_file(tape)
