import argparse
from cmd import Cmd

from tape.audio.modulation import AudioDemodulator
from tape.audio.signal_in import AudioInput
from tape.file.api import ByteListener, TapeFileListener, TapeFile
from tape.file.block import BlockPrinter, BlockParser
from tape.file.bytes import ByteDecoder
from tape.file.tapefile import TapeFilePrinter, TapeFileLoader
from tape.repository.api import TapeFileRepository
from tape.repository.repo_db import ZodbRepository


class ByteListenerStub(ByteListener):
    def __init__(self):
        self.values = []

    def process_byte(self, value: int):
        self.values.append(value)


class TapeFileHandler(TapeFileListener):
    def __init__(self, repository: TapeFileRepository):
        self.repository = repository

    def process_file(self, file: TapeFile):
        self.repository.add_tape_file(file)


class TapeShell(Cmd):
    def __init__(self, repository: TapeFileRepository):
        super().__init__()
        self.prompt = 'tape> '
        self.repository = repository

    def do_exit(self, input):
        return True

    def emptyline(self):
        pass

    def do_play(self, input):
        # file = file_load(targetfile)
        # play_file(file, None)
        print('Playing', input)

    def do_ls(self, input):
        for tape_file in self.repository.get_tape_files():
            print('%-8s  - %8d bytes' % (tape_file.fname, len(tape_file.fbody)))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run an interactive tape shell session.')
    parser.add_argument('--device', help='Select a device index.', type=int)
    parser.add_argument('dbname', nargs='?', type=str, help='Name of the tape database to use.')
    args = parser.parse_args()

    repository = ZodbRepository(args.dbname, observers=[])

    file_printer = TapeFilePrinter()
    file_handler = TapeFileHandler(repository)
    block_printer = BlockPrinter()

    file_loader = TapeFileLoader([file_printer, file_handler])
    block_parser = BlockParser([block_printer, file_loader])
    byte_decoder = ByteDecoder([block_parser])
    demodulation = AudioDemodulator([byte_decoder], rate=44100)

    audio_in = AudioInput([demodulation], daemon=True, device=args.device)
    audio_in.start()

    shell = TapeShell(repository)
    shell.cmdloop()
