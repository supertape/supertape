from unittest import TestCase

from tape.basic.minification import minify_basic, variable_name_generator, extract_variable_names


class TestMinification(TestCase):
    def test_null_process(self):
        self.assertEqual(minify_basic(''), '')

    def test_spaces(self):
        self.assertEqual(minify_basic('1 PRINT "HELLO WORLD !"\n'),
                         '1PRINT"HELLO WORLD !"\n')

    def test_variable_names(self):
        self.assertEqual(minify_basic('1 LANG=12\n2 LANGER=LANG\n3 GER=LANG\n'),
                         '1B=12\n2A=B\n3C=B\n')

        self.assertEqual(minify_basic('1 A=0:B=0:C=0:D=0\n'),
                         '1A=0:B=0:C=0:D=0\n')

    def test_variable_generation(self):
        it = variable_name_generator([])

        for i in range(26):
            self.assertEqual(chr(ord('A') + i), it.__next__())

        for i in range(26):
            self.assertEqual('A' + chr(ord('A') + i), it.__next__())

        for i in range(26):
            self.assertEqual('B' + chr(ord('A') + i), it.__next__())

    def test_variable_generation_blacklist(self):
        it = variable_name_generator(['B', 'D'])

        self.assertEqual('A', it.__next__())
        self.assertEqual('C', it.__next__())
        self.assertEqual('E', it.__next__())
        self.assertEqual('F', it.__next__())

    def test_variable_detection(self):
        self.assertEqual(extract_variable_names('1 PRI=1\n2 PRINT PRI\n'), ['PRI'])
        self.assertEqual(extract_variable_names('1 IFG=4 THEN GOTO 4\n'), ['G'])
        self.assertEqual(extract_variable_names('1 REM X=4\n'), [])
        self.assertEqual(extract_variable_names('1 PRINT "X=4"\n'), [])
        self.assertEqual(extract_variable_names('1 INPUT"YOUR GUESS";GUESS\n'), ['GUESS'])
        self.assertEqual(extract_variable_names('1 A=0:B=0:C=0:D=0\n'), ['A', 'B', 'C', 'D'])
        self.assertEqual(extract_variable_names('1 D=0:C=0:B=0:A=0\n'), ['A', 'B', 'C', 'D'])
        self.assertEqual(extract_variable_names('1 A=0:AA=0:B=0:C=0:D=0\n'), ['A', 'AA', 'B', 'C', 'D'])

    def test_variable_integrity(self):
        self.assertEqual(minify_basic('1 PRI=1\n2 PRINT PRI\n'),
                         '1A=1\n2PRINTA\n')

        self.assertEqual(minify_basic('1 OT=1\n2 PRINT OT\n3 GOTO 1\n'),
                         '1A=1\n2PRINTA\n3GOTO1\n')

        self.assertEqual(minify_basic('1 INPUT"YOUR GUESS";GUESS\n'),
                         '1INPUT"YOUR GUESS";A\n')
