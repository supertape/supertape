from typing import List
from unittest import TestCase

import imageio
import numpy as np

from tape.ui.console.image_to_basic import load_and_resize_image, alice_modes, generate_image_data, \
    generate_image_data_flat, patterns, generate_image_data_dithered


class TestImageToBasic(TestCase):

    def test_included(self):
        # This pseudo-test is here to assess behavior of the "in" operator on numpy arrays
        matrix_2d = np.array([[1, 2], [3, 4], [5, 6]])

        self.assertIn([1, 2], matrix_2d)
        self.assertIn([1, 3], matrix_2d)
        self.assertIn([9, 2], matrix_2d)
        self.assertNotIn([2, 1], matrix_2d)
        self.assertNotIn([7, 8], matrix_2d)

        self.assertIn([1, 2], matrix_2d.tolist())
        self.assertNotIn([1, 3], matrix_2d.tolist())
        self.assertNotIn([2, 1], matrix_2d.tolist())
        self.assertNotIn([7, 8], matrix_2d.tolist())

        self.assertTrue(np.any(np.equal(matrix_2d, [1, 2]).all(1)))
        self.assertFalse(np.any(np.equal(matrix_2d, [1, 3]).all(1)))

    def test_patterns(self):
        p4 = patterns((2, 2, 3), 7)

        self.assertEqual(p4.shape, (16, 2, 2))

        self.assertIn([[0, 0], [0, 0]], p4.tolist())
        self.assertIn([[0, 7], [0, 0]], p4.tolist())
        self.assertIn([[7, 0], [7, 0]], p4.tolist())

        self.assertNotIn([[8, 0], [0, 0]], p4.tolist())

    def test_image_loading(self):

        for dither in ['dithered', 'flat']:
            mode = alice_modes[40]

            for pic in ['black', 'gradient', 'gray', 'white', 'lenna']:
                buffer = load_and_resize_image(f'images/{pic}.png', mode)

                if dither == 'flat':
                    alice_indexes = generate_image_data_flat(buffer, mode)
                else:
                    alice_indexes = generate_image_data_dithered(buffer, mode)

                out = np.zeros((mode.height, mode.width, 3), dtype=np.uint8)

                for y in range(mode.height):
                    for x in range(mode.width):
                        out[y][x] = mode.palette[alice_indexes[y][x][0]]

                imageio.imwrite(f'images/{pic}-{dither}.png', out)

                data = generate_image_data(alice_indexes, mode)

                with open(f'images/{pic}-{dither}-data.txt', 'w') as file:
                    file.write(str(data))
